<?php

namespace Drupal\bibcite_import_oai\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines import form.
 */
class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bibcite_import_oai_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bibcite_import_oai.settings');
    $url_list = $config->get('url');

    $form['url'] = [
      '#type'           => 'checkboxes',
      '#options'        => array_combine($url_list, $url_list),
      '#title'          => $this->t('URLs to import from'),
      '#default_value'  => $url_list,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // User must have selected at least one URL to import.
    $selected = array_filter($form_state->getValue('url'), function ($item) {
      return !empty($item);
    });
    if (empty($selected)) {
      $form_state->setErrorByName('url', $this->t('Must select at least one URL to import from'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected = array_filter($form_state->getValue('url'), function ($item) {
      return !empty($item);
    });
    $operations = [];
    foreach ($selected as $url) {
      $operations[] = [
        '\Drupal\bibcite_import_oai\Model\Import::processImportBatch',
        [$url],
      ];
    }

    // Create a batch to process the selected URLs to import.
    $batch = [
      'title'       => $this->t('Importing from OAI'),
      'operations'  => $operations,
      'finished'    => '\Drupal\bibcite_import_oai\Model\Import::processImportFinished',
    ];
    batch_set($batch);
  }

}
