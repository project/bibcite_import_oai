<?php

namespace Drupal\bibcite_import_oai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines bibcite_import_oai configuration form.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bibcite_import_oai_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bibcite_import_oai.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bibcite_import_oai.settings');

    $form['#tree'] = TRUE;

    // Initial number of URLs.
    if (!$form_state->get('num_url')) {
      if (!empty(count($config->get('url')))) {
        $form_state->set('num_url', count($config->get('url')));
      }
      else {
        $form_state->set('num_url', 1);
      }
    }
    // Container for URLs.
    $url_values = $config->get('url');
    $form['url_container'] = [
      '#type' => 'container',
      '#prefix' => "Go to oai interface such as<br>
      <strong>https://your.repo.url/oai/openaire?verb=ListSets</strong><br>
      find the Institution you would like to connect and copy / paste the url from 'Records'<br>
      Should look something like:<br>
      <strong>https://your.repo.url/oai/openaire?verb=ListRecords&metadataPrefix=oai_dc&set=com_10362_410</strong><br>",
    ];

    for ($i = 0; $i < $form_state->get('num_url'); $i++) {
      $form['url_container'][$i]['url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('URL @num', ['@num' => ($i + 1)]),
        '#default_value' => $url_values[$i] ?? '',
      ];
    }

    $form['add_url'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another URL'),
    ];

    $form['use_cron'] = [
      '#prefix' => str_repeat('', 2) . '<h2>' . $this->t('Auto update') . '</h2>',
      '#suffix' => $this->t('Select this option to update publications from the sources every night.'),
      '#type' => 'checkbox',
      '#title' => $this->t('Update daily'),
      '#default_value' => $config->get('use_cron'),
    ];

    $form['actions']['save_add_lesson'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration & go to Import'),
      '#weight' => 1000,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Determine if saving config or adding new URL.
    switch ($values['op']) {
      case 'Add another URL':
        $this->addNewUrl($form, $form_state);
        break;

      default:
        // Assemble the URLs array.
        $url = [];
        if (!empty($values['url_container'])) {
          foreach ($values['url_container'] as $url_value) {
            if (!empty($url_value['url'])) {
              $url[] = $url_value['url'];
            }
          }
        }
        $this->config('bibcite_import_oai.settings')
          ->set('use_cron', $values['use_cron'])
          ->set('url', $url)
          ->save();

        parent::submitForm($form, $form_state);

        // Redirect to oai-import if update now is selected.
        if ($values['op'] == 'Save configuration & go to Import') {
          $form_state->setRedirect('bibcite_import_oai.index');
        }
    }
  }

  /**
   * Adds new URL field to the form.
   *
   * @param array $form
   *   Form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  private function addNewUrl(array &$form, FormStateInterface $form_state) {
    // Add 1 to the number of URLs.
    $num_url = $form_state->get('num_url');
    $form_state->set('num_url', ($num_url + 1));
    // Rebuild the form.
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Validate only when saving.
    if ($values['op'] != 'Add another URL') {
      if (!empty($values['url_container'])) {
        // Validate that the URLs are well formed.
        foreach ($values['url_container'] as $idx => $url_value) {
          // Skip empty values.
          if (!empty($url_value['url'])) {
            if (!filter_var($url_value['url'], FILTER_VALIDATE_URL)) {
              $form_state->setErrorByName(
                'url_container][' . $idx,
                $this->t('Invalid URL'));
            }
          }
        }
      }
      parent::validateForm($form, $form_state);
    }
  }

}
