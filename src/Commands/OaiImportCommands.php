<?php

namespace Drupal\bibcite_import_oai\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush commands.
 */
class OaiImportCommands extends DrushCommands {

  /**
   * Imports data from an OAI URL.
   *
   * @param array $options
   *   Command options.
   *
   * @command bibcite_import_oai:import
   *
   * @option url URL for OAI XML data
   * @aliases oai,oai-import
   */
  public function import(array $options = ['url' => NULL]) {
    $url = $options['url'];
    if (empty($url)) {
      throw new \Exception('Error: Please specify URL.');
    }
    else {
      // Filter the URL value.
      $url = filter_var($url, FILTER_VALIDATE_URL);
      if ($url === FALSE) {
        throw new \Exception('Error: URL is invalid.');
      }
      $importer = \Drupal::service('bibcite_import_oai.importer');
      try {
        $imported = $importer->importDataFromUrl($url);
        $this->output->writeln(sprintf('Imported %s references', $imported));
      }
      catch (\Exception $e) {
        throw new \Exception($e->getMessage());
      }
    }
  }

  /**
   * Deletes all references from bibcite.
   *
   * @command bibcite_import_orcid:delete_refs
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_refs
   */
  public function deleteRefs() {
    $refs_to_delete = \Drupal::entityQuery('bibcite_reference')
      ->accessCheck(FALSE)
      ->execute();
    echo "Deleting " . count($refs_to_delete) . " References.\n";
    $storage_handler = \Drupal::entityTypeManager()->getStorage("bibcite_reference");
    $entities = $storage_handler->loadMultiple($refs_to_delete);
    $storage_handler->delete($entities);
  }

  /**
   * Deletes all contributors from bibcite.
   *
   * @command bibcite_import_orcid:delete_contribs
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_contribs
   */
  public function deleteContribs() {
    $contribs_to_delete = \Drupal::entityQuery('bibcite_contributor')
      ->accessCheck(FALSE)
      ->execute();
    echo "Deleting " . count($contribs_to_delete) . " Conntributors.\n";
    $storage_handler = \Drupal::entityTypeManager()->getStorage("bibcite_contributor");
    $entities = $storage_handler->loadMultiple($contribs_to_delete);
    $storage_handler->delete($entities);
  }

  /**
   * Deletes all entities from bibcite.
   *
   * @command bibcite_import_orcid:delete_all
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_all
   */
  public function deleteAll() {

    // Delete References.
    $this->deleteRefs();

    // Delete Contributors.
    $this->deleteContribs();
  }

}
