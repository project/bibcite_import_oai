<?php

namespace Drupal\bibcite_import_oai\Model;

/**
 * Reference Data as Imported from OAI. To be inserted as Bibcite reference.
 */
class OaiReference {

  /**
   * Document title.
   *
   * @var string
   */
  public $title;

  /**
   * Document secondary title.
   *
   * @var string
   */
  public $secondaryTitle;

  /**
   * Document authors.
   *
   * @var array
   */
  public $authors;

  /**
   * Document publication year.
   *
   * @var string
   */
  public $year;

  /**
   * Document abstract.
   *
   * @var string
   */
  public $abstract;

  /**
   * Document keywords.
   *
   * @var array
   */
  public $keywords;

  /**
   * Document type.
   *
   * @var string
   */
  public $type;

  /**
   * Work type.
   *
   * @var string
   */
  public $workType;

  /**
   * Document ISSN number.
   *
   * @var string
   */
  public $issn;

  /**
   * Document ISBN number.
   *
   * @var string
   */
  public $isbn;

  /**
   * Document DOI number.
   *
   * @var string
   */
  public $doi;

  /**
   * Locators URL.
   *
   * @var string
   */
  public $url;

  /**
   * Document publisher.
   *
   * @var string
   */
  public $publisher;

}
