<?php

namespace Drupal\bibcite_import_oai\Model;

use Drupal\bibcite_entity\Entity\Contributor;
use Drupal\bibcite_entity\Entity\Keyword;
use Drupal\bibcite_entity\Entity\Reference;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Model for the import of OAI data.
 */
class Import {

  /**
   * Drupal state key to store the last cron import timestamp.
   */
  const CRON_LAST_RUN_STATE_KEY = 'bibcite_import_oai_cron_last_run';

  /**
   * Watchdog channel to be used when logging activity.
   */
  const LOG_CHANNEL = 'bibcite_import_oai';

  /**
   * Search an existing entity by name.
   *
   * @param mixed $name
   *   Entity name to search.
   * @param string $entity_type
   *   Author last name.
   *
   * @return mixed
   *   Referenced entity.
   */
  public function getReferencedEntity($name, string $entity_type) {
    $entity = NULL;
    switch ($entity_type) {
      case 'author':
        $query = \Drupal::entityQuery('bibcite_contributor')
          ->accessCheck(TRUE)
          ->condition('first_name', $name['first_name'])
          ->condition('last_name', $name['last_name']);
        $id = current($query->execute());
        if (!empty($id)) {
          $entity = Contributor::load($id);
        }
        break;

      case 'keyword':
        $query = \Drupal::entityQuery('bibcite_keyword')
          ->accessCheck(TRUE)
          ->condition('name', $name);
        $id = current($query->execute());
        if (!empty($id)) {
          $entity = Keyword::load($id);
        }
        break;

      default:
        break;
    }
    return $entity;
  }

  /**
   * Creates new referenced entity, returning the created object.
   *
   * @param mixed $name
   *   Entity name.
   * @param string $entity_type
   *   Entity type.
   *
   * @return mixed
   *   Created entity
   */
  public function createReferencedEntity($name, string $entity_type) {
    switch ($entity_type) {
      case 'author':
        $entity = Contributor::create([
          'type' => 'entity_contributor',
          'first_name' => $name['first_name'],
          'last_name' => $name['last_name'],
        ]);
        break;

      case 'keyword':
        $entity = Keyword::create([
          'type' => 'entity_keyword',
          'name' => $name,
        ]);
        break;

      default:
        $entity = NULL;
        break;
    }
    if (!empty($entity)) {
      $entity->save();
    }
    return $entity;
  }

  /**
   * Perform necessary transformations on referenced item name.
   *
   * @param string $name
   *   Original name as obtained from Scopus.
   * @param string $entity_type
   *   Entity type to determine transformation needed.
   *
   * @return mixed
   *   Processed entity name.
   */
  protected function processReferenceName(string $name, string $entity_type) {
    switch ($entity_type) {
      case 'author':
        $name_parts = explode(', ', $name);
        // Some authors have only one name, comma separated.
        $only_one_part = count($name_parts) == 1;
        $processed = [
          'first_name'  => $only_one_part ? $name_parts[0] : $name_parts[1],
          'last_name'   => $only_one_part ? '' : $name_parts[0],
        ];
        break;

      default:
        $processed = $name;
    }
    return $processed;
  }

  /**
   * Obtain referenced entities by name.
   *
   * @param array $names
   *   List of names to process.
   * @param string $entity_type
   *   Type of entity to process.
   *
   * @return array
   *   Array of entities.
   */
  protected function getReferencedItemsFromNames(array $names, string $entity_type): array {
    $entities = [];
    if (!empty($names)) {
      foreach ($names as $name) {
        // Some entities need the name to be processed.
        $name_processed = $this->processReferenceName($name, $entity_type);
        $entity = $this->getReferencedEntity($name_processed, $entity_type);
        if (empty($entity)) {
          $entity = $this->createReferencedEntity($name_processed, $entity_type);
        }
        $entities[] = $entity;
      }
    }
    return $entities;
  }

  /**
   * Obtains the XML Document to be parsed from an URL.
   *
   * @param string $url
   *   URL to download OAI XML data.
   *
   * @return \DOMDocument
   *   Document as DOMDocument object to be parsed.
   */
  protected function getDocumentFromUrl($url): \DOMDocument {
    // Initialize cURL session.
    $ch = curl_init();
    // Define cURL options.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 600);
    // Receive the result of the cURL session.
    $xml_data = curl_exec($ch);
    if ($xml_data === FALSE) {
      throw new \Exception('Failed to retrieve data from URL: ' . curl_error($ch));
    }
    $xml_data = curl_exec($ch);
    if ($xml_data === FALSE) {
      throw new \Exception('Failed to retrieve data from URL: ' . curl_error($ch));
    }
    $response = curl_getinfo($ch);
    if ($response['http_code'] != 200) {
      // This will extract the timing information.
      // Create metrics variables from getinfo.
      extract(curl_getinfo($ch));
      // Request this time explicitly.
      $appconnect_time = curl_getinfo($ch, CURLINFO_APPCONNECT_TIME);
      // format, to get rid of scientific notation.
      $downloadduration = number_format($total_time - $starttransfer_time, 9);
      $namelookup_time = number_format($namelookup_time, 9);
      $metrics = "CURL...: $url Time...: $total_time DNS: $namelookup_time Connect: $connect_time SSL/SSH: $appconnect_time PreTransfer: $pretransfer_time StartTransfer: $starttransfer_time Download: $downloadduration";
      error_log($metrics);

      throw new \Exception(sprintf(
        'Invalid response code from server. Expected 200, got %s. ',
        $response['http_code']
      ));
    }

    curl_close($ch);
    $doc = new \DOMDocument();
    $doc->loadXML($xml_data);
    if ($doc === FALSE) {
      throw new \Exception('Response could not be parsed as XML.');
    }
    return $doc;
  }

  /**
   * Finds a bibcite reference from it's URl.
   *
   * @param string $url
   *   URL to search.
   *
   * @return Drupal\bibcite_entity\Entity\Reference
   *   Reference object requested.
   */
  public function getReferenceByUrl(string $url) {
    $query = \Drupal::entityQuery('bibcite_reference')
      ->accessCheck(TRUE)
      ->condition('bibcite_url', $url);
    $id = current($query->execute());
    return Reference::load($id);
  }

  /**
   * Saves reference entity.
   *
   * @param OaiReference $item
   *   Item parsed from OAI.
   */
  protected function saveReference(OaiReference $item) {
    $uid = \Drupal::currentUser()->id();
    $authors = $this->getReferencedItemsFromNames($item->authors, 'author');
    $keywords = $this->getReferencedItemsFromNames($item->keywords, 'keyword');
    $title = $this->trimString($item->title, 250);
    $reference_data = [
      'type' => $item->type,
      'title' => $title,
      'bibcite_secondary_title' => $item->secondaryTitle,
      'bibcite_year' => $item->year,
      'bibcite_abst_e' => $item->abstract,
      'author' => $authors,
      'contribs_atts' => $attributes ?? NULL,
      'bibcite_type_of_work' => $item->workType,
      'bibcite_url' => $item->url,
      'bibcite_doi' => $item->doi,
      'bibcite_issn' => $item->issn,
      'bibcite_isbn' => $item->isbn,
      'bibcite_publisher' => $item->publisher,
      'keywords' => $keywords,
      'bibcite_publisher' => $item->publisher,
      'uid' => $uid,
    ];
    // Verifies if reference already exists, and updates if so.
    $reference = $this->getReferenceByUrl($item->url);
    if (empty($reference)) {
      $reference = Reference::create($reference_data);
    }
    else {
      foreach ($reference_data as $key => $value) {
        if (isset($value)) {
          $reference->set($key, $value);
        }
      }
    }
    $reference->save();
  }

  /**
   * Assembles the URL for fetching paginated data.
   *
   * @param string $url
   *   Original URL.
   * @param string $pagination
   *   resumptionToken sting.
   *
   * @return string
   *   URL for next page
   */
  protected function assemblePaginationUrl(string $url, string $pagination): string {
    $url_parts = parse_url($url);
    return sprintf(
      '%s://%s%s?verb=ListRecords&resumptionToken=%s',
      $url_parts['scheme'],
      $url_parts['host'],
      $url_parts['path'],
      $pagination
    );
  }

  /**
   * Trims a tring to a maxinum length.
   *
   * @param string $string
   *   String to trim.
   * @param int $length
   *   Maximum length.
   *
   * @return string
   *   Trimmed string.
   */
  protected function trimString(string $string, int $length): string {
    return substr($string, 0, $length);
  }

  /**
   * Imports OAI data from URL provided.
   *
   * @param string $url
   *   OAI URL to fetch data.
   *
   * @return int
   *   Number of inserted references.
   */
  public function importDataFromUrl($url): int {
    $data = [];
    $done_fetching = FALSE;
    $steps = 0;
    $max_steps = 100;
    // Log start of import.
    $start_log_message = sprintf('Starting OAI import from %s.', $url);
    \Drupal::logger(self::LOG_CHANNEL)->notice($start_log_message);
    do {
      $steps++;
      $parser = new OaiParser($this->getDocumentFromUrl($url));
      $data = array_merge($data, $parser->parseOaiData());
      $pagination = $parser->getPaginationData();
      if (empty($pagination)) {
        $done_fetching = TRUE;
      }
      else {
        $url = $this->assemblePaginationUrl($url, $pagination);
      }
    } while ((!$done_fetching) && ($steps < $max_steps));

    if (!empty($data)) {
      foreach ($data as $item) {
        $this->saveReference($item);
      }
    }
    // Log end of import.
    $imported_items_count = count($data);
    $end_log_message = sprintf(
      'Finished OAI import from %s, %s items imported',
      $url,
      $imported_items_count);
    \Drupal::logger(self::LOG_CHANNEL)->notice($end_log_message);
    return $imported_items_count;
  }

  /**
   * Imports data from OAI in batches.
   *
   * @param string $url
   *   URL to import from.
   * @param array $context
   *   Batch context.
   */
  public static function processImportBatch(string $url, array &$context) {
    $context['message'] = new TranslatableMarkup(
      'Importing from @current_url',
      ['@current_url' => $url],
      []);
    $instance = new self();
    try {
      $imported = $instance->importDataFromUrl($url);
      $context['results'][] = new TranslatableMarkup(
        'Imported @current_count entries from @current_url.',
        [
          '@current_count' => $imported,
          '@current_url' => $url,
        ]);
    }
    catch (\Exception $e) {
      \Drupal::logger(self::LOG_CHANNEL)->error($e->getMessage());
    }
  }

  /**
   * Batch Import finished callback.
   *
   * @param bool $sucess
   *   Sucess status flag.
   * @param array $result
   *   Batch operation result.
   * @param array $operations
   *   Operations array, probably useless ;).
   */
  public static function processImportFinished(bool $sucess, array $result, array $operations) {
    if ($sucess) {
      \Drupal::messenger()->addMessage(new TranslatableMarkup('Import successfull.'));
      foreach ($result as $result_row) {
        \Drupal::messenger()->addMessage($result_row);
      }
    }
    else {
      \Drupal::messenger()->addError('Could not import data.');
    }
  }

}
