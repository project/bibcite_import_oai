# Bibcite Import OAI

The OAI module provides a convenient way to import repositories using D-Space
via OAI (Open Archives Initiative)
protocol.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bibcite_import_oai).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/bibcite_import_oai).


## Table of Contents

- Requirements
- Installation
- Configuration
  - Use the Web Interface
  - Use Drush
- Maintainers
- Supporting Organizations


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

Configure the module by adding the desired collection URLs in the web interface
settings page.


### Use the Web Interface

1. Go to the main page: `/admin/bibcite_import_oai`
1. Navigate to the settings page and add the URLs from the collections you want to
   retrieve: `/admin/bibcite_import_oai/config`
    - Optionally, configure auto-update settings.
1. Visit the import page: `/admin/oai-import/import` and press 'Import'


### Use Drush

Use the following Drush command to import repositories:

```
bash
drush oai-import:import --url="url"
```

An alias 'oai' is available. Example:

```
bash
drush oai:import --url="https://[your_site]/oai/openaire?verb=ListRecords&metadataPrefix=oai_dc&set=com_1822_428"
```

## Maintainers

- Ricardo Marcelino - [rfmarcelino](https://www.drupal.org/u/rfmarcelino)

## Supporting Organizations:

- Development time [Omibee](https://www.drupal.org/omibee)
